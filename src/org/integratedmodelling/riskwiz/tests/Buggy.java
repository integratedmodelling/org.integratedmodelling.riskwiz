package org.integratedmodelling.riskwiz.tests;


import java.io.FileInputStream;

import org.integratedmodelling.riskwiz.bn.BNNode;
import org.integratedmodelling.riskwiz.bn.BeliefNetwork;
import org.integratedmodelling.riskwiz.inference.ls.JoinTreeCompiler;
import org.integratedmodelling.riskwiz.io.genie.GenieReader;
import org.integratedmodelling.riskwiz.io.xmlbif.XmlBifReader;
import org.integratedmodelling.riskwiz.jtree.JTInference;


public class Buggy {

    /**
     * @param args
     */
    public static void main(String[] args) {
		
        GenieReader r = new GenieReader();
        XmlBifReader rb = new XmlBifReader();

        try {
        	
            System.out.println("ciao");
            BeliefNetwork bn = r.load(new FileInputStream("examples/water.xdsl"));
            // BeliefNetwork bn =rb.load(new FileInputStream("examples/sprinkler.xml"));

            JTInference inference = new JTInference();

            inference.initialize(bn, new JoinTreeCompiler());
            // inference.setObservation("Cloudy", "true");
            inference.run();
    		
            for (BNNode n : bn.vertexSet()) {
                System.out.println(n.getName() + ": " + n.getMarginal());
            }
        
        } catch (Exception e) {

            e.printStackTrace();
        }
        
    }

}
