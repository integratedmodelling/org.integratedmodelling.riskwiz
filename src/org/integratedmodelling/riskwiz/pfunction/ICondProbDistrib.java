
package org.integratedmodelling.riskwiz.pfunction;


import java.util.List;

import org.nfunk.jep.ParseException;


/**
 * An interface which all user-defined conditional probability
 * distributions (CPDs) 
 * are expected to implement.
 */
public interface ICondProbDistrib {

    /**
     * For a discrete distribution, returns the conditional probability of 
     * <code>childValue</code> given the argument values <code>args</code>.  
     * For a continuous distribution, returns the conditional probability 
     * density at <code>childValue</code>.   
     * @throws ParseException 
     */
    double getProb(List args, Object childValue) throws ParseException;

    /**
     * Returns the natural log of the value returned by getProb.
     * @throws ParseException 
     */
    double getLogProb(List args, Object childValue) throws ParseException;

    /**
     * Samples a value according to this CPD given the <code> args
     * </code>.   
     * @throws ParseException 
     */
    Object sampleVal(List args) throws ParseException; 
}

