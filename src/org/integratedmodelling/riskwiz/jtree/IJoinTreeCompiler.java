package org.integratedmodelling.riskwiz.jtree;


import org.integratedmodelling.riskwiz.bn.BeliefNetwork;
import org.integratedmodelling.riskwiz.debugger.IJTCompilerDebugger;


public interface IJoinTreeCompiler<V extends JTVertex> {

    public abstract IJoinTree<V> execute(BeliefNetwork beliefNetwork) throws Exception;
    public abstract IJoinTree<V> execute(BeliefNetwork beliefNetwork, IJTCompilerDebugger deb) throws Exception;

}
