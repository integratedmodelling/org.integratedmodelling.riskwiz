package org.integratedmodelling.riskwiz.jtree;


import org.integratedmodelling.riskwiz.bn.BeliefNetwork;


public interface IJoinTreeDecisionCompiler <V extends JTVertex> {
    public abstract IJoinTreeDecision<V> execute(BeliefNetwork beliefNetwork) throws Exception;
}
